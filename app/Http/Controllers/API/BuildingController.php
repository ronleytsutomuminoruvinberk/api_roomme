<?php

namespace App\Http\Controllers\API;

use Validator;

use App\Models\Building;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\API\APITemplateController as APITemplate;

class BuildingController extends APITemplate
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result_building = Building::all();
        return $this->sendResponse(200, $result_building->toArray());
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_input = $request->only('name', 'description');

        if ($request_input['description'] == '') {
            $request_input['description'] = 'Description not found';
        }

        $validator = Validator::make($request_input, [
            'name' => 'required|unique:buildings',
            'description' => 'nullable'
            ]);
            
        if($validator->fails()){
            return $this->sendError(400, $validator->errors());     
        }

        $request_input['slug'] = str_slug($request_input['name'], '-');
        $request_input['created_by'] = Auth::user()->usertype;

        try {
            if (Building::create($request_input)) {
                return $this->sendResponse(201, '', 'Building has been successfully saved');
            }
            else {
               throw new Exception('Building can\'t saved. Please try again!');
            }
        } catch (Exception $err) {
            return $this->sendError(500, $err->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function show(Building $building, $slug)
    {
        $result_building = $building::where('slug', $slug)->first();
        return $this->sendResponse(200, $result_building->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Building $building, $id)
    {
        $result_building = $building::findOrFail($id);

        $request_input = $request->only('name', 'description');

        if ($request_input['description'] == '') {
            $request_input['description'] = 'Description not found';
        }

        $request_input['slug'] = str_slug($request_input['name'], '-');
        $request_input['updated_by'] = Auth::user()->usertype;

        $validator = Validator::make($request_input, [
            'name' => 'required',
            'description' => 'nullable'
        ]);

        if($validator->fails()){
            return $this->sendError(400, $validator->errors());     
        }
        
        try {
            if ($result_building->update($request_input)) {
                return $this->sendResponse(201, '', 'Building has been successfully updated');
            }
            else {
                throw new Exception('Building can\'t updated. Please try again!');
            }
        } catch (Exception $err) {
            return $this->sendError(500, $err->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Building $building, $id) 
    {
        $result_building = $building::findOrFail($id);
        try {
            if ($result_building->delete()) {
                return $this->sendResponse(201, '', 'Building has been successfully deleted');
            }
            else {
                throw new Exception('Building can\'t deleted. Please try again!');
            }
        } catch (Exception $err) {
            return $this->sendError(500, $err->getMessage());
        }
    }
}
